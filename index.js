const axios = require("axios");

const PORTAL_URL = process.env.PORTAL_URL || "https://portal.kreative.im";

exports.verifyKey = (req, res, next) => {
    const key = req.headers["portal_key"];
    const ksn = req.headers["portal_ksn"];
    const aidn = req.headers["portal_aidn"];
    
    const data = {key, ksn, aidn};

    axios.post(PORTAL_URL+'/api/accounts/verify', data)
    .catch(error => {
        const status = error.status;
        const errorCode = error.data.data.errorCode;

        res.status(status).json({status, data:{errorCode}});
    })
    .then(response => {
        res.locals.ccn = response.data.data;
        res.locals.ksn = ksn;
        res.locals.aidn = aidn;
        
        next();
    });
};